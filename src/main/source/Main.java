import graphics.MainWindow;

public class Main {
    /**
     * Runs the game.
     */
    public static void main(String[] args) {

        new MainWindow();
    }
}
