package gameLogic.Models;

import gameLogic.Models.abstractions.CharacterBase;

import java.util.ArrayList;
import java.util.List;

/**
 * Handles the player party.
 */
public class Player {
    public List<CharacterBase> characters;

    public Player(){
        characters = new ArrayList<>();
    }
}
