package gameLogic.Models;

import gameLogic.Models.abstractions.CharacterBase;
import gameLogic.data.Skills;
import gameLogic.data.CharacterClass;
import java.util.ArrayList;
/**
 * Handles the characters of the player.
 */
public class PlayerCharacter implements CharacterBase{
    private String name;
    private int maxHP;
    private int actualHP;
    private int damage;
    private ArrayList<Skills> skills;

    public PlayerCharacter(String name, CharacterClass characterClass){
        this.name=name;
        this.maxHP=characterClass.HP;
        this.actualHP=this.maxHP;
        this.damage=characterClass.damage;
        this.skills=characterClass.skills;
    }

    public void Attack(){

    }
}
