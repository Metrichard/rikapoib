package gameLogic.Models;

import gameLogic.Models.abstractions.*;
import gameLogic.Models.abstractions.Enemy;
import gameLogic.Models.abstractions.IRoom;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
/**
 * Generates the rooms of the map, handles the containments.
 */
public class Room implements IRoom {
    private Player playerPlace;
    private List<Enemy> enemyPlace;
    public Boolean IsVisible;
    public Boolean IsDiscovered;
    private List<UUID> AdjacentRooms;
    private RoomType roomType;
    private final UUID identifier;

    public Room(RoomType roomType){
        IsVisible = false;
        IsDiscovered = false;
        enemyPlace = new ArrayList<>();
        AdjacentRooms = new ArrayList<>();
        identifier = UUID.randomUUID();
    }

    /**
     * @return true if there are adjacent rooms.
     */
    public Boolean HasNeighbour() { return AdjacentRooms.size() > 0; }

    /**
     * @return true if the room can have more neighbours.
     */
    public Boolean CanHaveMoreNeighbours() { return AdjacentRooms.size() < 3; }

    public UUID GetIdentifier() { return identifier; }

    public void SetPlayer(Player player){
        playerPlace = player;
    }

    /**
     * Removes the player from the room.
     */
    public Player PlayerLeave(){
        IsDiscovered = true;
        Player player = playerPlace;
        playerPlace = null;
        return player;
    }

    public void AddNeighbour(IRoom room){
        if(AdjacentRooms.contains(room.GetIdentifier()))
            return;
        AdjacentRooms.add(room.GetIdentifier());
    }

    /**
     * Check if the added room is a neighbour
     * @param target the target room we want to go to
     * @return if neighbour we return true if not false
     */
    public boolean IsNeighbour(IRoom target) {
        for(UUID roomId : AdjacentRooms){
           if(roomId == target.GetIdentifier()){
               return true;
           }
        }
        return false;
    }
}
