package gameLogic.Models;

public enum RoomType {
    // fight
    Red,
    // random
    Blue,
    // next level
    Yellow,
    // no fight
    Green,
    // Empty
    White
}
