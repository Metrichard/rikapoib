package gameLogic.Models.abstractions;

import gameLogic.Models.Player;
import java.util.UUID;

public interface IRoom {
    void SetPlayer(Player player);
    Player PlayerLeave();
    void AddNeighbour(IRoom room);
    boolean IsNeighbour(IRoom target);
    UUID GetIdentifier();
    Boolean HasNeighbour();
    Boolean CanHaveMoreNeighbours();
}
