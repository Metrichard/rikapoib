package gameLogic.data;

import java.util.ArrayList;

public enum CharacterClass {
    WARRIOR(20,10, new ArrayList<Skills>()),
    MAGE(15,5, new ArrayList<Skills>()),
    KNIGHT(25,8, new ArrayList<Skills>()),
    CLERIC(15,5, new ArrayList<Skills>());

    public final int HP;
    public final int damage;
    public final ArrayList<Skills> skills;

    private CharacterClass(int HP, int damage, ArrayList<Skills> skills){
        this.HP = HP;
        this.damage = damage;
        this.skills = skills;
    }
}
