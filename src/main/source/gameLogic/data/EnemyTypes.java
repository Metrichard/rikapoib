package gameLogic.data;

public enum EnemyTypes {
    GOBLIN("Goblin",10),
    WOLF("Wolf",8),
    ORC("Orc",15);

    public final String name;
    public final int hp;

    private EnemyTypes(String name, int hp){
        this.name = name;
        this.hp = hp;
    }
}
