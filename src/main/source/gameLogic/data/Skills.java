package gameLogic.data;

public enum Skills {
    BASICATTACK,
    FIREBALL,
    FROSTBOLT,
    TAUNT,
    HEAL;
}
