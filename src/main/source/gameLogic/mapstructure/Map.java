package gameLogic.mapstructure;

import gameLogic.Models.*;
import gameLogic.Models.abstractions.IRoom;
import gameLogic.mapstructure.abstraction.IMap;

import java.util.*;

/**
 * Generates the game map and handles the rooms in it.
 */
public class Map implements IMap {
    private List<IRoom> Rooms;

    public List<IRoom> GetRooms(){
        return Rooms;
    }

    /**
     * Creates the rooms list fills it up and adds the neighbouring rooms randomly
     * @param numberOfRooms how many rooms are to be generated in the map
     */
    @Override
    public void GenerateRoomsAndNeighbours(int numberOfRooms){
        Rooms = new ArrayList<>(numberOfRooms);

        //TODO: We have to make this more accurate for now this will work but there should be like rules we have to execute when we generate the rooms
        for(int i = 0; i < numberOfRooms; ++i){
            RoomType roomType = RoomType.White;
            int roomTypeNum = (int) Math.floor(Math.random() % 5);
            switch (roomTypeNum) {
                case 0 -> roomType = RoomType.Red;
                case 1 -> roomType = RoomType.Blue;
                case 2 -> roomType = RoomType.Yellow;
                case 3 -> roomType = RoomType.Green;
                case 4 -> roomType = RoomType.White;
            }
            Rooms.add(new Room(roomType));
        }
        Random rand = new Random();
        for(int i = 0; i < numberOfRooms; ++i){
            IRoom currentRoom = Rooms.get(i);
            for (IRoom room : Rooms) {
                if(rand.nextInt(100) < 20
                        && currentRoom.GetIdentifier() != room.GetIdentifier()
                        && currentRoom.CanHaveMoreNeighbours()
                        && room.CanHaveMoreNeighbours()){
                    currentRoom.AddNeighbour(room);
                    room.AddNeighbour(currentRoom);
                }
            }
        }

        //So there are no rooms which have 0 neighbours
        for(IRoom lonelyRoom : Rooms){
            if(!lonelyRoom.HasNeighbour()){
                for (IRoom room : Rooms){
                    if(!lonelyRoom.IsNeighbour(room) && room.CanHaveMoreNeighbours()){
                        room.AddNeighbour(lonelyRoom);
                        lonelyRoom.AddNeighbour(room);
                        break;
                    }
                }
            }
        }
    }

    /**
     *  This basically executes a movement between two rooms
     * @param source from where we want to com from
     * @param target to where we want to go
     * @return true if the movement is successful false if not
     */
    @Override
    public Boolean Movement(IRoom source, IRoom target) {
        if(source.IsNeighbour(target)){
            Player player = source.PlayerLeave();
            target.SetPlayer(player);
            return true;
        }
        return false;
    }
}
