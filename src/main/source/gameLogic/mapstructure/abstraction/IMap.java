package gameLogic.mapstructure.abstraction;

import gameLogic.Models.abstractions.IRoom;

import java.util.List;

public interface IMap {
    void GenerateRoomsAndNeighbours(int numberOfRooms);
    Boolean Movement(IRoom source, IRoom target);
    List<IRoom> GetRooms();
}
