package graphics;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import connection.GameEngine;

/**
 *Opens the game window and handles the panels on it.
 */
public class MainWindow extends JFrame {
    private final JPanel cardPane;
    private CardLayout card;

    private JPanel homeScreenPanel;
    private JLabel startNewGameBTN;

    private JLabel exitGameBTN;

    private JPanel newGameScreenPanel;
    private NewGameScreen newGameScreen;
    private JPanel menu;
    private GameEngine gameEngine;
    private Dimension dim;

    public MainWindow(){
        setTitle("Game Window");
        setResizable(false);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        dim = new Dimension(800, 600);
        card = new CardLayout();
        cardPane = new JPanel();
        cardPane.setLayout(card);
        add(cardPane);

        gameEngine = new GameEngine();
        initHomeScreen();
        initNewGameScreen();

        cardPane.add(homeScreenPanel, "homeScreenPanel");
        cardPane.add(newGameScreenPanel, "newGameScreenPanel");
        
        card.show(cardPane, "homeScreenPanel");
        
        pack();
        setVisible(true);
    }

    /**
     *Generates the panel of the home screen, with a new game button and an end game button.
     */
    private void initHomeScreen(){
        homeScreenPanel = new JPanel();
        homeScreenPanel.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();

        homeScreenPanel.setBackground(new Color(224,224,224));
        homeScreenPanel.setPreferredSize(dim);

        startNewGameBTN = new JLabel("Start new game");
        startNewGameBTN.setFont(startNewGameBTN.getFont().deriveFont((float) 40));
        startNewGameBTN.setForeground(Color.BLACK);
        gbc.gridx = 0;
        gbc.gridy = 0;
        homeScreenPanel.add(startNewGameBTN, gbc);

        exitGameBTN = new JLabel("Exit game");
        exitGameBTN.setFont(exitGameBTN.getFont().deriveFont((float) 40));
        exitGameBTN.setForeground(Color.BLACK);
        gbc.gridx = 0;
        gbc.gridy = 1;
        homeScreenPanel.add(exitGameBTN, gbc);
        
        startNewGameBTN.addMouseListener(new HomeScreenMouseListener(startNewGameBTN));
        exitGameBTN.addMouseListener(new HomeScreenMouseListener(exitGameBTN));   
    }

    /**
     * Generates the panel of the new game screen.
     */
    private void initNewGameScreen(){
        newGameScreenPanel = new JPanel();
        newGameScreenPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 0));
        
        newGameScreen = new NewGameScreen(gameEngine);
        menu = new JPanel();
        
        menu.setPreferredSize(new Dimension(400, 640));
        menu.setBackground(new Color(245, 245, 245));
    }

    /**
     * The mouse listener handler for the home screen.
     */
    class HomeScreenMouseListener implements MouseListener {
        private final JLabel label;

        public HomeScreenMouseListener(JLabel label) {
            this.label = label;
        }

        /**
         * Changes the font to red and increases the size if the cursor is on the text.
         */
        public void mouseEntered(MouseEvent e) {
            this.label.setFont(this.label.getFont().deriveFont((float) 80));
            this.label.setForeground(Color.RED);
        }

        /**
         * Changes the font to black and decreases the size if the cursor leaves the text.
         */
        public void mouseExited(MouseEvent e) {
            this.label.setFont(this.label.getFont().deriveFont((float) 40));
            this.label.setForeground(Color.BLACK);
        }

        /**
         * Handles the clicking events on the buttons.
         */
        public void mouseClicked(MouseEvent e) {
            if(this.label.getText().equals("Start new game")){
                System.out.println("Az új játék elindul!");
                card.show(cardPane, "newGameScreenPanel");
            }else if(this.label.getText().equals("Exit game")){
                System.exit(0);
            }
        }

        public void mousePressed(MouseEvent e) {}
        public void mouseReleased(MouseEvent e) {}
    }
}