package graphics;

import connection.GameEngine;

import javax.swing.*;

/**
 * Creates the graphics for the new game settings.
 */
public class NewGameScreen extends JPanel {
    private GameEngine gameEngine;

    public NewGameScreen(GameEngine gameEngine){
        this.gameEngine = gameEngine;
    }
}
