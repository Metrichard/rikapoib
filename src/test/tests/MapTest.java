package tests;

import gameLogic.Models.abstractions.IRoom;
import gameLogic.mapstructure.Map;
import gameLogic.mapstructure.abstraction.IMap;
import org.junit.*;
import java.util.Random;

public class MapTest {
    // TODO: write some tests for the room generation and the character generation, maybe later when we add more logic
    private static IMap map;

    @BeforeClass
    public static void SetUp(){
        IMap mapForTest = new Map();
        map = mapForTest;
    }

    @Test
    public void GenerateRoomsAndNeighbours_PassingInNumber_CheckingForGeneratedRoomNumber(){
        for(int i = 4; i < 10; i++){
            map.GenerateRoomsAndNeighbours(i);
            Assert.assertEquals(map.GetRooms().size(), i);
        }
    }

    @Test
    public void GenerateRoomsAndNeighbour_PassingInNumber_CheckingIfEveryoneHasNeighbours(){
        int numberOfRooms = new Random().nextInt(10);
        map.GenerateRoomsAndNeighbours(numberOfRooms);
        for(int i = 0; i < numberOfRooms; i++){
            IRoom room = map.GetRooms().get(i);
            Assert.assertEquals(room.HasNeighbour(), true);
        }
    }
}
